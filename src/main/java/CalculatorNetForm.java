
import org.apache.commons.lang3.StringUtils;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Natthakritta
 */
public class CalculatorNetForm extends javax.swing.JFrame {

    /**
     * Creates new form CalculatorNetForm
     */
    public CalculatorNetForm() {
        initComponents();
        setComboBoxSubnet();
    }

    public void showResult() {
        txt1.setText(txtIPAddress1.getText() + "." + txtIPAddress2.getText() + "." + txtIPAddress3.getText() + "." + txtIPAddress4.getText());
        txt2.setText(NetworkAddress());
        txt3.setText(UsableHost());
        txt4.setText(BroadcastAddress());
        txt5.setText(TotalNum());
        txt6.setText(Integer.toString(NumifUsable()));
        txt7.setText(SubnetMask());
        txt8.setText(WildcardMask());
        txt9.setText(BinarySubnetMask());
        txt10.setText(IPClass());
        txt11.setText("/" + CIDR());
        txt13.setText(txtIPAddress1.getText() + "." + txtIPAddress2.getText() + "." + txtIPAddress3.getText() + "." + txtIPAddress4.getText() + " /" + CIDR());
        txt14.setText(binary());
        txt15.setText(IntegerID().toString());
        txt16.setText("0x" + HexID());
    }

    public void setComboBoxSubnet() {
        ComboBoxSubnet.addItem("");
        ComboBoxSubnet.addItem("255.255.255.255 /32");
        ComboBoxSubnet.addItem("255.255.255.254 /31");
        ComboBoxSubnet.addItem("255.255.255.252 /30");
        ComboBoxSubnet.addItem("255.255.255.248 /29");
        ComboBoxSubnet.addItem("255.255.255.240 /28");
        ComboBoxSubnet.addItem("255.255.255.224 /27");
        ComboBoxSubnet.addItem("255.255.255.192 /26");
        ComboBoxSubnet.addItem("255.255.255.128 /25");
        ComboBoxSubnet.addItem("255.255.255.0 /24");
        ComboBoxSubnet.addItem("255.255.254.0 /23");
        ComboBoxSubnet.addItem("255.255.252.0 /22");
        ComboBoxSubnet.addItem("255.255.248.0 /21");
        ComboBoxSubnet.addItem("255.255.240.0 /20");
        ComboBoxSubnet.addItem("255.255.224.0 /19");
        ComboBoxSubnet.addItem("255.255.192.0 /18");
        ComboBoxSubnet.addItem("255.255.128.0 /17");
        ComboBoxSubnet.addItem("255.255.0.0 /16");
        ComboBoxSubnet.addItem("255.254.0.0 /15");
        ComboBoxSubnet.addItem("255.252.0.0 /14");
        ComboBoxSubnet.addItem("255.248.0.0 /13");
        ComboBoxSubnet.addItem("255.240.0.0 /12");
        ComboBoxSubnet.addItem("255.224.0.0 /11");
        ComboBoxSubnet.addItem("255.192.0.0 /10");
        ComboBoxSubnet.addItem("255.128.0.0 /9");
        ComboBoxSubnet.addItem("255.0.0.0 /8");
        ComboBoxSubnet.addItem("254.0.0.0 /7");
        ComboBoxSubnet.addItem("248.0.0.0 /6");
        ComboBoxSubnet.addItem("240.0.0.0 /5");
        ComboBoxSubnet.addItem("224.0.0.0 /4");
        ComboBoxSubnet.addItem("192.0.0.0 /2");
        ComboBoxSubnet.addItem("128.0.0.0 /1");
    }

    public String NetworkAddress() {
        String networkAddress = "";
        int IPAddress1 = Integer.parseInt(txtIPAddress1.getText());
        int IPAddress2 = Integer.parseInt(txtIPAddress2.getText());
        int IPAddress3 = Integer.parseInt(txtIPAddress3.getText());
        int IPAddress4 = Integer.parseInt(txtIPAddress4.getText());

        String subnet1 = "";
        String subnet2 = "";
        String subnet3 = "";
        String subnet4 = "";

        int octet1 = 0;
        int octet2 = 0;
        int octet3 = 0;
        int octet4 = 0;

        int count = 0;
        int index = 0;

        for (int i = 0; i < 4; i++) {
            count++;
            if (count == 1) {
                for (int j = index; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet1 = subnet1 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 2) {
                for (int j = index + 1; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet2 = subnet2 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 3) {
                for (int j = index + 1; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet3 = subnet3 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 4) {
                for (int j = index + 1; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet4 = subnet4 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            }
        }

        octet1 = Integer.parseInt(subnet1);
        octet2 = Integer.parseInt(subnet2);
        octet3 = Integer.parseInt(subnet3);
        octet4 = Integer.parseInt(subnet4);

        octet1 = octet1 & IPAddress1;
        octet2 = octet2 & IPAddress2;
        octet3 = octet3 & IPAddress3;
        octet4 = octet4 & IPAddress4;

        networkAddress = Integer.toString(octet1) + "." + Integer.toString(octet2) + "." + Integer.toString(octet3) + "." + Integer.toString(octet4);

        return networkAddress;
    }

    public String UsableHost() {
        String usableHost = "";
        String wildcardMask = "";

        int IPAddress1 = Integer.parseInt(txtIPAddress1.getText());
        int IPAddress2 = Integer.parseInt(txtIPAddress2.getText());
        int IPAddress3 = Integer.parseInt(txtIPAddress3.getText());
        int IPAddress4 = Integer.parseInt(txtIPAddress4.getText());
        IPAddress4 = IPAddress4 + 1;

        for (int i = 0; i < BinarySubnetMask().length(); i++) {
            if (BinarySubnetMask().charAt(i) == '1') {
                wildcardMask += "0";
            } else if (BinarySubnetMask().charAt(i) == '0') {
                wildcardMask += "1";
            } else {
                wildcardMask += ".";
            }
        }
        String subnet1 = "";
        String subnet2 = "";
        String subnet3 = "";
        String subnet4 = "";
        int index = 0;
        int count = 0;
        int octet1 = 0;
        int octet2 = 0;
        int octet3 = 0;
        int octet4 = 0;

        for (int i = 0; i < 4; i++) {
            count++;
            if (count == 1) {
                for (int j = 0; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet1 = subnet1 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 2) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet2 = subnet2 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 3) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet3 = subnet3 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }

            } else if (count == 4) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet4 = subnet4 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            }
        }
        octet1 = Integer.parseInt(subnet1, 2);
        octet2 = Integer.parseInt(subnet2, 2);
        octet3 = Integer.parseInt(subnet3, 2);
        octet4 = Integer.parseInt(subnet4, 2);

        octet1 = octet1 | IPAddress1;
        octet2 = octet2 | IPAddress2;
        octet3 = octet3 | IPAddress3;
        octet4 = octet4 | IPAddress4;
        octet4 = octet4 - 1;

        usableHost = Integer.toString(IPAddress1) + "." + Integer.toString(IPAddress2) + "." + Integer.toString(IPAddress3) + "." + Integer.toString(IPAddress4) + " - " + Integer.toString(octet1) + "." + Integer.toString(octet2) + "." + Integer.toString(octet3) + "." + Integer.toString(octet4);

        return usableHost;
    }

    public String TotalNum() {
        String host = "";
        String usable = "";
        int CIDR = Integer.parseInt(CIDR());
        switch (CIDR) {
            case 1:
                host = "2,147,483,648";
                break;
            case 2:
                host = "1,073,741,824";
                break;
            case 3:
                host = "536,870,912";
                break;
            case 4:
                host = "268,435,456";
                break;
            case 5:
                host = "134,217,728";
                break;
            case 6:
                host = "67,108,864";
                break;
            case 7:
                host = "33,554,432";
                break;
            case 8:
                host = "16,777,216";
                break;
            case 9:
                host = "8,388,608";
                break;
            case 10:
                host = "4,194,304";
                break;
            case 11:
                host = "2,097,152";
                break;
            case 12:
                host = "1,048,576";
                break;
            case 13:
                host = "524,288";
                break;
            case 14:
                host = "262,144";
                break;
            case 15:
                host = "131,072";
                break;
            case 16:
                host = "65,536";
                break;
            case 17:
                host = "32,768";
                break;
            case 18:
                host = "16,384";
                break;
            case 19:
                host = "8,192";
                break;
            case 20:
                host = "4,096";
                break;
            case 21:
                host = "2,048";
                break;
            case 22:
                host = "1,024";
                break;
            case 23:
                host = "512";
                break;
            case 24:
                host = "256";
                break;
            case 25:
                host = "128";
                break;
            case 26:
                host = "64";
                break;
            case 27:
                host = "32";
                break;
            case 28:
                host = "16";
                break;
            case 29:
                host = "8";
                break;
            case 30:
                host = "4";
                break;
            case 31:
                host = "2";
                break;
            case 32:
                host = "1";
                break;
        }
        return host;
    }

    public int NumifUsable() {
        String host = "";
        String usable = "";
        int CIDR = Integer.parseInt(CIDR());
        switch (CIDR) {
            case 1:
                host = "2147483648";
                break;
            case 2:
                host = "1073741824";
                break;
            case 3:
                host = "536870912";
                break;
            case 4:
                host = "268435456";
                break;
            case 5:
                host = "134217728";
                break;
            case 6:
                host = "67108864";
                break;
            case 7:
                host = "33554432";
                break;
            case 8:
                host = "16777216";
                break;
            case 9:
                host = "8388608";
                break;
            case 10:
                host = "4194304";
                break;
            case 11:
                host = "2097152";
                break;
            case 12:
                host = "1048576";
                break;
            case 13:
                host = "524288";
                break;
            case 14:
                host = "262144";
                break;
            case 15:
                host = "131072";
                break;

            case 16:
                host = "65536";
                break;
            case 17:
                host = "32768";
                break;
            case 18:
                host = "16384";
                break;
            case 19:
                host = "8192";
                break;
            case 20:
                host = "4096";
                break;
            case 21:
                host = "2048";
                break;
            case 22:
                host = "1024";
                break;
            case 23:
                host = "512";
                break;

            case 24:
                host = "256";
                break;
            case 25:
                host = "128";
                break;
            case 26:
                host = "64";
                break;
            case 27:
                host = "32";
                break;
            case 28:
                host = "16";
                break;
            case 29:
                host = "8";
                break;
            case 30:
                host = "4";
                break;
            case 31:
                host = "2";
                break;
            case 32:
                host = "1";
                break;
        }
        int num = Integer.parseInt(host);
        if (num > 1) {
            num = num - 2;
            return num;
        } else {
            return 0;
        }

    }

    public String BroadcastAddress() {
        String broadcastAddress = "";
        String wildcardMask = "";

        int IPAddress1 = Integer.parseInt(txtIPAddress1.getText());
        int IPAddress2 = Integer.parseInt(txtIPAddress2.getText());
        int IPAddress3 = Integer.parseInt(txtIPAddress3.getText());
        int IPAddress4 = Integer.parseInt(txtIPAddress4.getText());

        for (int i = 0; i < BinarySubnetMask().length(); i++) {
            if (BinarySubnetMask().charAt(i) == '1') {
                wildcardMask += "0";
            } else if (BinarySubnetMask().charAt(i) == '0') {
                wildcardMask += "1";
            } else {
                wildcardMask += ".";
            }
        }
        String subnet1 = "";
        String subnet2 = "";
        String subnet3 = "";
        String subnet4 = "";
        int index = 0;
        int count = 0;
        int octet1 = 0;
        int octet2 = 0;
        int octet3 = 0;
        int octet4 = 0;

        for (int i = 0; i < 4; i++) {
            count++;
            if (count == 1) {
                for (int j = 0; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet1 = subnet1 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 2) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet2 = subnet2 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 3) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet3 = subnet3 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }

            } else if (count == 4) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet4 = subnet4 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            }
        }
        octet1 = Integer.parseInt(subnet1, 2);
        octet2 = Integer.parseInt(subnet2, 2);
        octet3 = Integer.parseInt(subnet3, 2);
        octet4 = Integer.parseInt(subnet4, 2);

        octet1 = octet1 | IPAddress1;
        octet2 = octet2 | IPAddress2;
        octet3 = octet3 | IPAddress3;
        octet4 = octet4 | IPAddress4;

        broadcastAddress = Integer.toString(octet1) + "." + Integer.toString(octet2) + "." + Integer.toString(octet3) + "." + Integer.toString(octet4);

        return broadcastAddress;
    }

    public String SubnetMask() {
        String subnet = ComboBoxSubnet.getSelectedItem().toString();
        String SubnetMask = "";
        for (int i = 0; i < subnet.length(); i++) {
            if (subnet.charAt(i) == ' ') {
                break;
            }
            SubnetMask += subnet.charAt(i);
        }
        return SubnetMask;
    }

    public String BinarySubnetMask() {
        String binarySubnetMask = "";
        String subnet1 = "";
        String subnet2 = "";
        String subnet3 = "";
        String subnet4 = "";
        int count = 0;
        int octet1 = 0;
        int octet2 = 0;
        int octet3 = 0;
        int octet4 = 0;
        String binaryOc1 = "";
        String binaryOc2 = "";
        String binaryOc3 = "";
        String binaryOc4 = "";
        int index = 0;

        for (int i = 0; i < 4; i++) {
            count++;
            if (count == 1) {
                for (int j = index; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet1 = subnet1 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 2) {
                for (int j = index + 1; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet2 = subnet2 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 3) {
                for (int j = index + 1; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet3 = subnet3 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 4) {
                for (int j = index + 1; j < SubnetMask().length(); j++) {
                    if (SubnetMask().charAt(j) != '.') {
                        subnet4 = subnet4 + SubnetMask().charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            }
        }
        octet1 = Integer.parseInt(subnet1);
        binaryOc1 = toBinary(octet1);

        octet2 = Integer.parseInt(subnet2);
        binaryOc2 = toBinary(octet2);

        octet3 = Integer.parseInt(subnet3);
        binaryOc3 = toBinary(octet3);

        octet4 = Integer.parseInt(subnet4);
        binaryOc4 = toBinary(octet4);

        binarySubnetMask = binaryOc1 + "." + binaryOc2 + "." + binaryOc3 + "." + binaryOc4;
        return binarySubnetMask;
    }

    public String WildcardMask() {
        String wildcardMask = "";
        for (int i = 0; i < BinarySubnetMask().length(); i++) {
            if (BinarySubnetMask().charAt(i) == '1') {
                wildcardMask += "0";
            } else if (BinarySubnetMask().charAt(i) == '0') {
                wildcardMask += "1";
            } else {
                wildcardMask += ".";
            }
        }
        String subnet1 = "";
        String subnet2 = "";
        String subnet3 = "";
        String subnet4 = "";
        int index = 0;
        int count = 0;
        int octet1 = 0;
        int octet2 = 0;
        int octet3 = 0;
        int octet4 = 0;

        for (int i = 0; i < 4; i++) {
            count++;
            if (count == 1) {
                for (int j = 0; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet1 = subnet1 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 2) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet2 = subnet2 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            } else if (count == 3) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet3 = subnet3 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }

            } else if (count == 4) {
                for (int j = index + 1; j < wildcardMask.length(); j++) {
                    if (wildcardMask.charAt(j) != '.') {
                        subnet4 = subnet4 + wildcardMask.charAt(j);
                    } else {
                        index = j;
                        break;
                    }
                }
            }
        }
        octet1 = Integer.parseInt(subnet1, 2);
        octet2 = Integer.parseInt(subnet2, 2);
        octet3 = Integer.parseInt(subnet3, 2);
        octet4 = Integer.parseInt(subnet4, 2);

        wildcardMask = Integer.toString(octet1) + "." + Integer.toString(octet2) + "." + Integer.toString(octet3) + "." + Integer.toString(octet4);

        return wildcardMask;
    }

    public String CIDR() {
        String subnet = ComboBoxSubnet.getSelectedItem().toString();
        String CIDR = "";
        String CIDRnew = "";
        for (int i = subnet.length() - 1; i > 0; i--) {
            if (subnet.charAt(i) == '/') {
                break;
            }
            CIDR += subnet.charAt(i);
        }
        for (int i = CIDR.length() - 1; i >= 0; i--) {
            CIDRnew += CIDR.charAt(i);
        }
        return CIDRnew;
    }

    public String IPClass() {
        String iPClass = "";
        int CIDR = Integer.parseInt(CIDR());
        if (CIDR > 7 && CIDR < 16) {
            iPClass = "A";
        } else if (CIDR > 15 && CIDR < 24) {
            iPClass = "B";
        } else if (CIDR > 23 && CIDR < 33) {
            iPClass = "C";
        }
        return iPClass;
    }

    public String binary() {
        int num1 = Integer.parseInt(txtIPAddress1.getText());
        int num2 = Integer.parseInt(txtIPAddress2.getText());
        int num3 = Integer.parseInt(txtIPAddress3.getText());
        int num4 = Integer.parseInt(txtIPAddress4.getText());
        String binary1 = toBinary(num1);
        String binary2 = toBinary(num2);
        String binary3 = toBinary(num3);
        String binary4 = toBinary(num4);
        String binary = binary1 + binary2 + binary3 + binary4;
        return binary;
    }

    public static String toBinary(int x) {
        return StringUtils.leftPad(Integer.toBinaryString(x), 8, '0');
    }

    public Long IntegerID() {
        Long integerID = Long.parseLong(binary(), 2);
        return integerID;
    }

    public String HexID() {
        String hexID = Long.toHexString(IntegerID());
        return hexID;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel4 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        ComboBoxSubnet = new javax.swing.JComboBox<>();
        txtIPAddress1 = new javax.swing.JTextField();
        btnCalculator = new javax.swing.JButton();
        btnClear = new javax.swing.JButton();
        txtIPAddress2 = new javax.swing.JTextField();
        txtIPAddress3 = new javax.swing.JTextField();
        txtIPAddress4 = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txt1 = new javax.swing.JLabel();
        txt2 = new javax.swing.JLabel();
        txt3 = new javax.swing.JLabel();
        txt4 = new javax.swing.JLabel();
        txt5 = new javax.swing.JLabel();
        txt6 = new javax.swing.JLabel();
        txt7 = new javax.swing.JLabel();
        txt8 = new javax.swing.JLabel();
        txt9 = new javax.swing.JLabel();
        txt10 = new javax.swing.JLabel();
        txt11 = new javax.swing.JLabel();
        txt12 = new javax.swing.JLabel();
        txt13 = new javax.swing.JLabel();
        txt14 = new javax.swing.JLabel();
        txt15 = new javax.swing.JLabel();
        txt16 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel4.setBackground(new java.awt.Color(204, 255, 255));

        jLabel1.setFont(new java.awt.Font("Segoe UI", 1, 24)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(0, 0, 0));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("IPv4 Subnet Calculator");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));

        jLabel2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Subnet :");

        jLabel3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("IP Address :");

        ComboBoxSubnet.setBackground(new java.awt.Color(204, 204, 255));
        ComboBoxSubnet.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        ComboBoxSubnet.setForeground(new java.awt.Color(51, 51, 51));
        ComboBoxSubnet.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBoxSubnetActionPerformed(evt);
            }
        });

        txtIPAddress1.setForeground(new java.awt.Color(51, 51, 51));
        txtIPAddress1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIPAddress1ActionPerformed(evt);
            }
        });

        btnCalculator.setBackground(new java.awt.Color(204, 255, 204));
        btnCalculator.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnCalculator.setForeground(new java.awt.Color(51, 51, 51));
        btnCalculator.setText("Calculator");
        btnCalculator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalculatorActionPerformed(evt);
            }
        });

        btnClear.setBackground(new java.awt.Color(255, 204, 204));
        btnClear.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btnClear.setForeground(new java.awt.Color(51, 51, 51));
        btnClear.setText("Clear");
        btnClear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearActionPerformed(evt);
            }
        });

        txtIPAddress2.setForeground(new java.awt.Color(51, 51, 51));
        txtIPAddress2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIPAddress2ActionPerformed(evt);
            }
        });

        txtIPAddress3.setForeground(new java.awt.Color(51, 51, 51));
        txtIPAddress3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIPAddress3ActionPerformed(evt);
            }
        });

        txtIPAddress4.setForeground(new java.awt.Color(51, 51, 51));
        txtIPAddress4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIPAddress4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(btnCalculator, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(txtIPAddress1, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtIPAddress2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtIPAddress3, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(txtIPAddress4, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(ComboBoxSubnet, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(21, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(ComboBoxSubnet, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtIPAddress1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIPAddress2, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIPAddress3, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtIPAddress4, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnClear, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCalculator, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel2.setBackground(new java.awt.Color(255, 204, 255));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("IP Address :");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Network Address : ");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Subnet Mask : ");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Wildcard Mask : ");

        jLabel8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Binary Subnet Mask : ");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("IP Class : ");

        jLabel10.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("CIDR Notation : ");

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("IP Type : ");

        jLabel12.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("Short : ");

        jLabel13.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setText("Binary ID : ");

        jLabel14.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel14.setForeground(new java.awt.Color(51, 51, 51));
        jLabel14.setText("Integer ID :");

        jLabel15.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel15.setForeground(new java.awt.Color(51, 51, 51));
        jLabel15.setText("Hex ID : ");

        jLabel16.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setText("Usable Host IP Range : ");

        jLabel17.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(51, 51, 51));
        jLabel17.setText("Broadcast Address :");

        jLabel18.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Total Number of Hosts :");

        jLabel19.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(51, 51, 51));
        jLabel19.setText("Number of Usable Hosts :");

        txt1.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt1.setForeground(new java.awt.Color(51, 51, 51));

        txt2.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt2.setForeground(new java.awt.Color(51, 51, 51));

        txt3.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt3.setForeground(new java.awt.Color(51, 51, 51));

        txt4.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt4.setForeground(new java.awt.Color(51, 51, 51));

        txt5.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt5.setForeground(new java.awt.Color(51, 51, 51));

        txt6.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt6.setForeground(new java.awt.Color(51, 51, 51));

        txt7.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt7.setForeground(new java.awt.Color(51, 51, 51));

        txt8.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt8.setForeground(new java.awt.Color(51, 51, 51));

        txt9.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt9.setForeground(new java.awt.Color(51, 51, 51));

        txt10.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt10.setForeground(new java.awt.Color(51, 51, 51));

        txt11.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt11.setForeground(new java.awt.Color(51, 51, 51));

        txt12.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt12.setForeground(new java.awt.Color(51, 51, 51));

        txt13.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt13.setForeground(new java.awt.Color(51, 51, 51));

        txt14.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt14.setForeground(new java.awt.Color(51, 51, 51));

        txt15.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt15.setForeground(new java.awt.Color(51, 51, 51));

        txt16.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        txt16.setForeground(new java.awt.Color(51, 51, 51));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel19, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel17, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel16, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel15, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel7, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel12, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt2, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt3, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt6, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt12, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt13, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt14, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt15, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE)
                    .addComponent(txt16, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 253, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txt1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txt2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel16)
                    .addComponent(txt3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txt4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel18)
                    .addComponent(txt5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel19)
                    .addComponent(txt6))
                .addGap(12, 12, 12)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(txt7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(txt8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(txt9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(txt10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(txt11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel11)
                    .addComponent(txt12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel12)
                    .addComponent(txt13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(txt14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel14)
                    .addComponent(txt15))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel15)
                    .addComponent(txt16))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ComboBoxSubnetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBoxSubnetActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBoxSubnetActionPerformed

    private void txtIPAddress1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIPAddress1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIPAddress1ActionPerformed

    private void btnCalculatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalculatorActionPerformed
        // TODO add your handling code here:
        showResult();
    }//GEN-LAST:event_btnCalculatorActionPerformed

    private void txtIPAddress2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIPAddress2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIPAddress2ActionPerformed

    private void txtIPAddress3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIPAddress3ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIPAddress3ActionPerformed

    private void txtIPAddress4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIPAddress4ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIPAddress4ActionPerformed

    private void btnClearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearActionPerformed
        // TODO add your handling code here:
        txtIPAddress1.setText("");
        txtIPAddress2.setText("");
        txtIPAddress3.setText("");
        txtIPAddress4.setText("");
    }//GEN-LAST:event_btnClearActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CalculatorNetForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CalculatorNetForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CalculatorNetForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CalculatorNetForm.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CalculatorNetForm().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> ComboBoxSubnet;
    private javax.swing.JButton btnCalculator;
    private javax.swing.JButton btnClear;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel txt1;
    private javax.swing.JLabel txt10;
    private javax.swing.JLabel txt11;
    private javax.swing.JLabel txt12;
    private javax.swing.JLabel txt13;
    private javax.swing.JLabel txt14;
    private javax.swing.JLabel txt15;
    private javax.swing.JLabel txt16;
    private javax.swing.JLabel txt2;
    private javax.swing.JLabel txt3;
    private javax.swing.JLabel txt4;
    private javax.swing.JLabel txt5;
    private javax.swing.JLabel txt6;
    private javax.swing.JLabel txt7;
    private javax.swing.JLabel txt8;
    private javax.swing.JLabel txt9;
    private javax.swing.JTextField txtIPAddress1;
    private javax.swing.JTextField txtIPAddress2;
    private javax.swing.JTextField txtIPAddress3;
    private javax.swing.JTextField txtIPAddress4;
    // End of variables declaration//GEN-END:variables
}
